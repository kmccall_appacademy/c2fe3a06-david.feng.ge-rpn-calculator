class RPNCalculator
  @numbers
  @value

  def initialize()
    @numbers = []
    @current_value = nil
  end

  def push(number)
    @numbers.push(number.to_f)
  end

  def check_stack
    raise Exception.new("calculator is empty") if @numbers.size < 2
  end

  def plus
    check_stack
    number2 = @numbers.pop
    number1 = @numbers.pop
    @numbers.push(number1 + number2)
  end

  def minus
    check_stack
    number2 = @numbers.pop
    number1 = @numbers.pop
    @numbers.push(number1 - number2)
  end

  def times
    check_stack
    number2 = @numbers.pop
    number1 = @numbers.pop
    @numbers.push(number1 * number2)
  end

  def divide
    check_stack
    number2 = @numbers.pop
    number1 = @numbers.pop
    @numbers.push(number1 / number2) #or number1.fdiv(number2)
  end

# could define a private method to consolidate four operations

  def value
    @numbers.last
  end

  def tokens(string)
    result = []
    string.split.each do |element|
        result << element.to_i if /\A[-+]?\d+\z/ === element #Integer(char)
        result << :+ if element == '+' #char.to_sym (to symbol)
        result << :- if element == '-'
        result << :* if element == '*'
        result << :/ if element == '/'
    end
    result
  end

  def evaluate(string)
    stack = tokens(string)
    while stack.size > 0
      current = stack.shift
      if current == :+
        plus
      elsif current == :-
        minus
      elsif current == :*
        times
      elsif current == :/
        divide
      else
        push(current)
      end
    end
    value
  end


end
